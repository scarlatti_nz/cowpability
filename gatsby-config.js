module.exports = {
  siteMetadata: {
    title: `Cowpability`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `@gatsbyjs`,
    menuLinks: [
      {
        name: 'home',
        link: '/',
      },
      {
        name: 'Interview Insights',
        link: '/interview-insights',
        icon: './src/images/Icon-Insights.jpg',
      },
      {
        name: 'Survey Insights',
        link: '/survey-insights',
        icon: './src/images/Icon-Insights.jpg',
      },
      {
        name: 'Modelling the Workforce',
        link: '/modelling-the-workforce',
        icon: './src/images/Icon-Insights.jpg',
      },
    ],
    interviewInsightsLinks: [
      {
        name: 'Farm Assistant learning needs',
        link: '/interview-insights',
      },
      {
        name: 'Workforce learning needs',
        link: '/workforce-learning-needs',
      },
    ],
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
