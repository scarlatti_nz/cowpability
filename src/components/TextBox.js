/* eslint-disable react/destructuring-assignment */
import React from 'react'

const TextBox = props => {
  // eslint-disable-next-line react/prop-types
  return <div>{props.children}</div>
}

// TextBox.propTypes = {
//   children.propTypes.node
// }

TextBox.defaultProps = {
  children: <span> </span>,
}

export default TextBox
