/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React from 'react'
import ReactEcharts from 'echarts-for-react'

const DoughutChart = props => {
  const getOptions = () => ({
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)',
    },
    series: [
      {
        name: props.name,
        type: 'pie',
        radius: ['55%', '80%'],
        avoidLabelOverlap: true,
        label: {
          normal: {
            show: true,
            position: 'outside',
          },
          emphasis: {
            show: true,
            textStyle: {
              fontWeight: 'bold',
            },
          },
        },
        labelLine: {
          normal: {
            lineStyle: {
              color: 'rgba(218, 223, 225, 1)',
            },
            smooth: 0.2,
            length: 15,
            length2: 25,
          },
        },
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)',
          },
        },
        data: props.data,
      },
    ],
  })
  return <ReactEcharts option={getOptions()} style={{ height: 350, width: 530 }} />
}
export default DoughutChart
