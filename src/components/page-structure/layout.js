/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import { useStaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'

// import Hero from "./hero"
import Header from './header'
import './layout.scss'

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  return (
    <>
      {/* <Hero /> */}
      <RootContainer>
        <Header
          menuLinks={data.site.siteMetadata.menuLinks}
          siteTitle={data.site.siteMetadata.title}
        />
        <LayoutContainer style={{}}>
          <main>{children}</main>
          <footer />
        </LayoutContainer>
      </RootContainer>
    </>
  )
}

const RootContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100vh;
  width: 100vw;
  background-color: rgba(236, 240, 241, 1);
`

const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 100%;
  /* max-width: 1080px; */
  padding-top: 0;
`

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
