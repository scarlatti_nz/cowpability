import { graphql, useStaticQuery } from 'gatsby'

import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'
import PageHeader from './page-header'
import Layout from './layout'

const PageLayout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query PageTitle {
      site {
        siteMetadata {
          interviewInsightsLinks {
            name
            link
          }
        }
      }
    }
  `)

  return (
    <>
      <Layout>
        <PageHeader interviewInsightsLinks={data.site.siteMetadata.interviewInsightsLinks} />
        <ColumnContainer>
          <main>{children}</main>
        </ColumnContainer>
      </Layout>
    </>
  )
}

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
`

PageLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default PageLayout
