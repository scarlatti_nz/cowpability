/* eslint-disable react/prop-types */
import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

// eslint-disable-next-line react/prop-types
const Header = ({ siteTitle, menuLinks }) => (
  <StyledHeader title={siteTitle}>
    <StyledLogo to="/">
      <h2>{siteTitle}</h2>
    </StyledLogo>
    <StyledContainer>
      <nav>
        <ul>
          {menuLinks.map(link => (
            <li key={link.name}>
              <HeaderLink to={link.link} activeClassName="active">
                <h4>{link.name}</h4>
                <img src={link.icon} alt="test" />
              </HeaderLink>
            </li>
          ))}
        </ul>
      </nav>
    </StyledContainer>
  </StyledHeader>
)

const StyledHeader = styled.header`
  display: flex;
  flex-direction: column;
  height: auto;
  width: 20%;
  min-width: 250px;
  background: rgb(111, 177, 57, 0.8);
`

const StyledContainer = styled.div`
  height: 100%;
  margin: 0 auto;
  padding: 1em;
  display: flex;
  flex-direction: column;
`

const StyledLogo = styled(Link)`
  color: white;
  text-decoration: none;
  text-align: center;
  font-size: 2em;
  margin-top: 2em;
  margin-bottom: 2em;
`

const HeaderLink = styled(Link)`
  color: white;
  text-decoration: none;
  font-size: 1em;
  padding: 25px 4px;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  /* &.active {
    color: #4b4b4b;
  }
  &.active:after {
    content: '';
    display: flex;
    padding-top: 0.5rem;
    flex-direction: row;
    align-self: baseline;
    padding-bottom: 0;
    width: 20%;
    border-right: 3px solid white;
  } */
`

Header.propTypes = {
  // eslint-disable-next-line react/require-default-props
  siteTitle: PropTypes.string,
}

// export default StyledHeaderSection
export default Header
