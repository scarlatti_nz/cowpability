/* eslint-disable react/prop-types */
import { Link } from 'gatsby'
import React from 'react'
import styled from 'styled-components'
// import PropTypes from "prop-types"

const PageHeader = ({ interviewInsightsLinks }) => (
  <StyledHeader>
    <nav>
      <StyledUL>
        {interviewInsightsLinks.map(link => (
          <StyledLI key={link.name}>
            <StyledLink to={link.link} activeClassName="active">
              <h4>{link.name}</h4>
            </StyledLink>
          </StyledLI>
        ))}
      </StyledUL>
    </nav>
  </StyledHeader>
)

const StyledHeader = styled.div`
  display: flex;
  flex-direction: row;
  background-color: white;
  /* box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.2); */
  height: 50px;
  margin-bottom: 2em;
  h4 {
    font-size: 12px;
  }
`

const StyledLink = styled(Link)`
  display: flex;
  color: #b1b1b1;
  text-decoration: none;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  margin-right: 2em;
  flex-direction: column;
  margin-top: 1em;
  &.active {
    color: #4b4b4b;
  }
  &.active:after {
    content: '';
    display: flex;
    padding-top: 0.5rem;
    flex-direction: column;
    align-self: baseline;
    padding-bottom: 0;
    width: 20%;
    border-bottom: 3px solid #7bab56;
  }
`

const StyledUL = styled.ul`
  display: flex;
  margin-bottom: 0;
  height: 100%;
`

const StyledLI = styled.li`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 0;
`

export default PageHeader
