import React from 'react'
import styled from 'styled-components'
import LearningNeeds from '../charts/LearningNeeds'
import PageLayout from '../components/page-structure/page-layout'
import SEO from '../components/seo'

const InterviewInsights = () => (
  <PageLayout>
    <SEO title="Interview Insights" />
    <ColumnContainer>
      <div>
        <SectionHeader>Farm Assistant Learning Needs</SectionHeader>
        <StyledTextContainer>
          <p>
            This analysis looks at the areas of training that farmers think farm asssistants require
            in their first six months, first year, and two years in the industry. Its displays, as a
            proportion of all knowledge or learning in the first six months on the job (or first
            year, …), how much is spent learning about animals, the environment, and so on. It shows
            the ‘average’ opinion of farm assistants and of employers/managers in regards to these
            learning needs.
          </p>
        </StyledTextContainer>
      </div>
      <SectionHeader>Proportion of all learning in each capability area</SectionHeader>
      <RowContainer>
        <LearningNeeds />
      </RowContainer>
    </ColumnContainer>
  </PageLayout>
)
const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`

const StyledTextContainer = styled.div`
  background-color: white;
  /* box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.2); */
  padding: 17.5px 20px;
  display: flex;
  margin: 0 auto;
  flex-direction: column;
  width: 1200px;
  p {
    line-height: 1.5rem;
  }
  margin-bottom: 3rem;
`

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  div {
    /* padding: 17.5px 30px; */
  }
`
const SectionHeader = styled.h3`
  padding: 1px 30px;
  padding-top: 1rem;
`
export default InterviewInsights
