import React, { Component } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import DoughnutChart from '../components/charts/doughnut-chart'
import PageLayout from '../components/page-structure/page-layout'
import SEO from '../components/seo'

const apiURL = 'http://127.0.0.1:5000/cna/'

const apiBroadEmployer = 'ln/?interviewee=Employer&broad=true'
const apiBroadEmployee = 'ln/?interviewee=FA&broad=true'

const apiTimePeriodSix = '&time_period=6+month'
const apiTimePeriodOneYear = '&time_period=1+year'
const apiTimePeriodTwoYear = '&time_period=2+year'

class InterviewInsights extends Component {
  constructor() {
    super()
    this.state = {
      employerData: [],
      employeeData: [],
      isButtonActive: false,
    }
  }

  componentDidMount() {
    this.fetchOverviewData()
    // this.fetchData(timePeriod)
  }

  fetchOverviewData = () => {
    axios
      .all([axios.get(`${apiURL}${apiBroadEmployer}`), axios.get(`${apiURL}${apiBroadEmployee}`)])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            employerData: employer.data,
            employeeData: employee.data,
          })
        }),
      )
    this.changeTimePeriod(1)
  }

  fetch6monthsData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodSix}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodSix}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            employerData: employer.data,
            employeeData: employee.data,
          })
        }),
      )
    this.changeTimePeriod(2)
  }

  fetchOneYearData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodOneYear}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodOneYear}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            employerData: employer.data,
            employeeData: employee.data,
          })
        }),
      )
    this.changeTimePeriod(3)
  }

  fetchTwoYearData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodTwoYear}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodTwoYear}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            employerData: employer.data,
            employeeData: employee.data,
          })
        }),
      )
    this.changeTimePeriod(4)
  }

  changeTimePeriod(event, buttonNumber) {
    event.preventDefault()
    this.setState({
      isButtonActive: buttonNumber,
    })
  }

  render() {
    const { employeeData } = this.state
    const { employerData } = this.state
    const { isButtonActive } = this.state
    return (
      <PageLayout>
        <SEO title="Interview Insights" />
        <ColumnContainer>
          <div>
            <SectionHeader>Farm Assistant Learning Needs</SectionHeader>
            <StyledTextContainer>
              <p>
                This analysis looks at the areas of training that farmers think farm asssistants
                require in their first six months, first year, and two years in the industry. Its
                displays, as a proportion of all knowledge or learning in the first six months on
                the job (or first year, …), how much is spent learning about animals, the
                environment, and so on. It shows the ‘average’ opinion of farm assistants and of
                employers/managers in regards to these learning needs.
              </p>
            </StyledTextContainer>
          </div>
          <SectionHeader>Proportion of all learning in each capability area</SectionHeader>
          <RowContainer>
            <ButtonContainer>
              <button
                className={isButtonActive === 1 ? 'active' : null}
                onClick={this.fetchOverviewData}
                type="button"
              >
                Overview
              </button>
              <button
                className={isButtonActive === 2 ? 'active' : null}
                onClick={this.fetch6monthsData}
                type="button"
              >
                6 Months
              </button>
              <button
                className={isButtonActive === 3 ? 'active' : null}
                onClick={this.fetchOneYearData}
                type="button"
              >
                1 Year
              </button>
              <button
                className={isButtonActive === 4 ? 'active' : null}
                onClick={this.fetchTwoYearData}
                type="button"
              >
                2 Years
              </button>
            </ButtonContainer>
            <ChartContainer>
              <FarmEmployeeChart>
                <h4>Farm Assistant&apos;s Opinion</h4>
                <DoughnutChart
                  name="Proportion of all learning in each capability area"
                  data={employeeData}
                />
              </FarmEmployeeChart>
              <FarmEmployerChart>
                <h4>Farm Manager&apos;s Opinion</h4>
                <DoughnutChart
                  name="Proportion of all learning in each capability area"
                  data={employerData}
                />
              </FarmEmployerChart>
            </ChartContainer>
          </RowContainer>
        </ColumnContainer>
      </PageLayout>
    )
  }
}

const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`

const StyledTextContainer = styled.div`
  background-color: white;
  /* box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.2); */
  padding: 17.5px 20px;
  display: flex;
  margin: 0 auto;
  flex-direction: column;
  width: 1200px;
  p {
    line-height: 1.5rem;
  }
  margin-bottom: 3rem;
`

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  div {
    /* padding: 17.5px 30px; */
  }
`
const SectionHeader = styled.h3`
  padding: 1px 30px;
  padding-top: 1rem;
`

const FarmEmployerChart = styled.div`
  height: 350px;
  width: 575px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h4 {
    text-transform: uppercase;
    letter-spacing: 0.15em;
    margin-top: 1rem;
  }
`

const FarmEmployeeChart = styled.div`
  height: 350px;
  width: 575px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h4 {
    text-transform: uppercase;
    letter-spacing: 0.15em;
    margin-top: 1rem;
  }
`

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 1rem;
  justify-content: space-between;
  width: 30%;
  height: 25px;
  button {
    height: 35px;
    border-radius: 1px;
    border: none;
    margin: 0;
    text-decoration: none;
    background: transparent;
    color: #b1b1b1;
    font-size: 1rem;
    cursor: pointer;
    text-align: center;
    transition: background 250ms ease-in-out, transform 150ms ease;
    -webkit-appearance: none;
    -moz-appearance: none;
    font-family: 'Lato', sans-serif;
  }
  .active {
    color: #333333;
    border: 1px solid #8659f5d1;
    background: transparent;
  }
`

const ChartContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 1200px;
  margin-bottom: 1rem;
`

export default InterviewInsights
