import React from 'react'

import Layout from '../components/page-structure/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hello world</h1>
    <div />
  </Layout>
)

export default IndexPage
