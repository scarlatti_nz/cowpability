import React from 'react'
import styled from 'styled-components'
import Layout from '../components/page-structure/layout'
import CurrentKnowledgeGaps from '../charts/CurrentKnowledgeGaps'

// import { Link } from "gatsby"

// import SEO from "../components/seo"

const SurveyInsights = () => (
  <Layout>
    <SectionHeader>Survey Insights Page</SectionHeader>
    <StyledTextContainer>
      <SectionHeader>Current knowledge gaps</SectionHeader>
      <p>
        This analysis looks at the areas of training that farmers think farm asssistants require in
        their first six months, first year, and two years in the industry. Its displays, as a
        proportion of all knowledge or learning in the first six months on the job (or first year,
        …), how much is spent learning about animals, the environment, and so on. It shows the
        ‘average’ opinion of farm assistants and of employers/managers in regards to these learning
        needs.
      </p>
    </StyledTextContainer>
    <CurrentKnowledgeGaps />
  </Layout>
)

const SectionHeader = styled.h3`
  padding: 17.5px 30px;
`
const StyledTextContainer = styled.div`
  background-color: white;
  /* box-shadow: 0px 3px 15px rgba(0, 0, 0, 0.2); */
  padding: 17.5px 20px;
  display: flex;
  margin: 0 auto;
  flex-direction: column;
  width: 1200px;
  p {
    line-height: 1.5rem;
  }
  margin-bottom: 3rem;
`
export default SurveyInsights
