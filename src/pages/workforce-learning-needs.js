// import Layout from "../components/layout"
// import { Link } from "gatsby"

import React from 'react'
import styled from 'styled-components'
import PageLayout from '../components/page-structure/page-layout'

// import SEO from "../components/seo"

const WorkforceLearningNeeds = () => (
  <PageLayout>
    <SectionHeader>Workforce Learning Needs Page</SectionHeader>
  </PageLayout>
)

const SectionHeader = styled.h3`
  padding: 17.5px 30px;
`
export default WorkforceLearningNeeds
