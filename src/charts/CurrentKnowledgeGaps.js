import React, { Component } from 'react'
import ReactEcharts from 'echarts-for-react'

import styled from 'styled-components'
import axios from 'axios'

const apiURL = 'http://127.0.0.1:5000/cna/'

const apiBroadEmployer = 'ln/?interviewee=Employer&broad=true'
const apiBroadEmployee = 'ln/?interviewee=FA&broad=true'

const apiTimePeriodSix = '&time_period=6+month'
const apiTimePeriodOneYear = '&time_period=1+year'
const apiTimePeriodTwoYear = '&time_period=2+year'

// const apiTimePeriods = ['&time_period=6+month', '&time_period=1+year', '&time_period=2+year']

export default class LearningNeeds extends Component {
  constructor(props) {
    super(props)
    this.state = {
      // employerData: [],
      // employeeData: [],
      // isButtonActive: false,
    }
    // this.changeTimePeriod = this.changeTimePeriod.bind(this)
  }

  componentDidMount() {
    this.fetchOverviewData()
    // this.fetchData(timePeriod)
  }

  fetchOverviewData = () => {
    axios
      .all([axios.get(`${apiURL}${apiBroadEmployer}`), axios.get(`${apiURL}${apiBroadEmployee}`)])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            // employerData: employer.data,
            // employeeData: employee.data,
          })
        }),
      )
    // this.changeTimePeriod(1)
  }

  // fetchData = timePeriod => {
  //   axios
  //     .all([
  //       axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriods}`),
  //       axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriods}`),
  //     ])
  //     .then(
  //       axios.spread((employer, employee) => {
  //         employer.data.map(farmEmployer => {
  //           // Round Values to 2 decimal points
  //           farmEmployer.value = farmEmployer.value.toFixed(2)
  //           return farmEmployer
  //         })
  //         employee.data.map(farmEmployee => {
  //           // Round Values to 2 decimal points
  //           farmEmployee.value = farmEmployee.value.toFixed(2)
  //           return farmEmployee
  //         })
  //         this.setState({
  //           employerData: employer.data,
  //           employeeData: employee.data,
  //         })
  //       }),
  //     )
  //   this.handleClick(2)
  // }

  fetch6monthsData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodSix}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodSix}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            // employerData: employer.data,
            // employeeData: employee.data,
          })
        }),
      )
    // this.changeTimePeriod(2)
  }

  fetchOneYearData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodOneYear}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodOneYear}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            // employerData: employer.data,
            // employeeData: employee.data,
          })
        }),
      )
    // this.changeTimePeriod(3)
  }

  fetchTwoYearData = () => {
    axios
      .all([
        axios.get(`${apiURL}${apiBroadEmployer}${apiTimePeriodTwoYear}`),
        axios.get(`${apiURL}${apiBroadEmployee}${apiTimePeriodTwoYear}`),
      ])
      .then(
        axios.spread((employer, employee) => {
          employer.data.map(farmEmployer => {
            // Round Values to 2 decimal points
            farmEmployer.value = farmEmployer.value.toFixed(2)
            return farmEmployer
          })
          employee.data.map(farmEmployee => {
            // Round Values to 2 decimal points
            farmEmployee.value = farmEmployee.value.toFixed(2)
            return farmEmployee
          })
          this.setState({
            // employerData: employer.data,
            // employeeData: employee.data,
          })
        }),
      )
    // this.changeTimePeriod(4)
  }

  getOptions = () => ({
    tooltip: {
      trigger: 'item',
      formatter: '{a} <br/>{b} : {c} ({d}%)',
    },
    // legend: {
    //   orient: 'vertical',
    //   x: 'left',
    //   data: ['At Expectation', 'Above Expectation', 'Below Expectation'],
    // },
    series: [
      {
        name: 'Proportion of all learning in each capability area',
        type: 'pie',
        radius: ['55%', '80%'],
        avoidLabelOverlap: true,
        label: {
          normal: {
            show: true,
            position: 'outside',
          },
          emphasis: {
            show: true,
            textStyle: {
              fontWeight: 'bold',
            },
          },
        },
        labelLine: {
          normal: {
            lineStyle: {
              color: 'rgba(218, 223, 225, 1)',
            },
            smooth: 0.2,
            length: 15,
            length2: 25,
          },
        },
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)',
          },
        },
        data: [
          { value: 13, name: 'Below Expectation', itemStyle: { color: '#de3f3a' } },
          { value: 65, name: 'At Expectation', itemStyle: { color: '#e36a24' } },
          { value: 22, name: 'Above Expectation', itemStyle: { color: '#39c46a' } },
        ],
      },
    ],
  })

  // changeTimePeriod(buttonNumber) {
  //   this.setState({
  //     isButtonActive: buttonNumber,
  //   })
  // }

  render() {
    // const { employeeData } = this.state
    // const { employerData } = this.state
    // const { isButtonActive } = this.state

    return (
      <ColumnContainer>
        <ChartContainer>
          <FarmEmployeeChart>
            <h4>Farm Assistant&apos;s Opinion</h4>
            <ReactEcharts option={this.getOptions()} style={{ height: 350, width: 530 }} />
          </FarmEmployeeChart>
          <FarmEmployerChart>
            <h4>Farm Manager&apos;s Opinion</h4>
            <ReactEcharts option={this.getOptions()} style={{ height: 350, width: 530 }} />
          </FarmEmployerChart>
        </ChartContainer>
      </ColumnContainer>
    )
  }
}

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
`

// const ButtonContainer = styled.div`
//   display: flex;
//   flex-direction: row;
//   margin-bottom: 1rem;
//   justify-content: space-between;
//   width: 30%;
//   height: 25px;
//   button {
//     height: 35px;
//     border-radius: 1px;
//     border: none;
//     margin: 0;
//     text-decoration: none;
//     background: transparent;
//     color: #b1b1b1;
//     font-size: 1rem;
//     cursor: pointer;
//     text-align: center;
//     transition: background 250ms ease-in-out, transform 150ms ease;
//     -webkit-appearance: none;
//     -moz-appearance: none;
//     font-family: 'Lato', sans-serif;
//   }
//   .active {
//     color: #333333;
//     border: 1px solid #8659f5d1;
//     background: transparent;
//   }
// `

const FarmEmployerChart = styled.div`
  height: 350px;
  width: 575px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h4 {
    text-transform: uppercase;
    letter-spacing: 0.15em;
    margin-top: 1rem;
  }
`

const FarmEmployeeChart = styled.div`
  height: 350px;
  width: 575px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  h4 {
    text-transform: uppercase;
    letter-spacing: 0.15em;
    margin-top: 1rem;
  }
`

const ChartContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 1200px;
  margin-bottom: 1rem;
`
