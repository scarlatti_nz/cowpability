import Typography from "typography"
import fairyGateTheme from "typography-theme-fairy-gates"

const typography = new Typography(
  {
    bodyFontFamily: ["Merriweather", "serif"],
    bodyWeight: "400px",
    headerFontFamily: ["Lato", "serif"],
    headerWeight: "500px"
  },
  fairyGateTheme
)
export const { scale, rhythm, options } = typography
export default typography
